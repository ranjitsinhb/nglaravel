<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use App\Traits\ColumnFillable;

class Employee extends Model
{
    use HasFactory;
    use Notifiable;
    use ColumnFillable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'employees';
    protected $primaryKey = 'id';
    public $incrementing = false;

    /**
     * Set id when creating new employee
     */
    protected static function boot(){
        parent::boot();
        Employee::creating(function ($model) {
            $model->setUserId();
        });
    }    

    public function setUserId(){
        $this->attributes['id'] = (string) Str::uuid();
    }

}
